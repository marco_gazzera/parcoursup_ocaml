open Utils
(*
Propriéte testé : Ajout en file d'attente / Modification de file d'attente / Ajout de proposition / 
                  refus automatique

Objectif : Ici Sandrine se verra proposé Haut Kramel, puis Etienne et Pierre se voient eux aussi proposé Haut Kramel
        Stephane et Julien seront rajoutés en liste d'attente.
        Lors du premier jour Sandrine refusera automatiquement Prepa Haut-Kramel car ses rang indique qu'elle préfére Prepa Eau-el
        Toutefois Stephane est déjà en liste d'attente, il devra donc passé en proposition lors du 2nd jour
        Julien gagnera donc une place dans sa file d'attente


Remarque : Tout se déroule comme prévu, dans un monde idéal Stephane n'aurai pas du passé en file d'attente puisque Sandrine refuse le même jour
de sa mise en file d'attente. Mais on ne rappel pas tout les élèves à chaque refuse automatique, il attend donc le 2nd jour pour la bonne nouvelle*)
let session_attente = nouvelle_session ()

let () =
  ajoute_candidat session_attente ~nom_candidat:"Sandrine";
ajoute_candidat session_attente ~nom_candidat:"Etienne" ;
ajoute_candidat session_attente ~nom_candidat:"Pierre" ;
ajoute_candidat session_attente ~nom_candidat:"Stephane" ;
ajoute_candidat session_attente ~nom_candidat:"Julien" ;
ajoute_formation session_attente ~nom_formation:"Prepa Haut-Kramel" ~capacite:3;
ajoute_formation session_attente ~nom_formation:"Prepa Eau-el" ~capacite:1;
ajoute_voeu session_attente ~rang_repondeur:(Some 1) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_attente ~rang_repondeur:(Some 0) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_attente ~rang_repondeur:(Some 0) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_attente ~rang_repondeur:(Some 0) ~nom_candidat:"Pierre" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_attente ~rang_repondeur:(Some 0) ~nom_candidat:"Stephane" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_attente ~rang_repondeur:(Some 0) ~nom_candidat:"Julien" ~nom_formation:"Prepa Haut-Kramel";
ajoute_commission session_attente ~nom_formation:"Prepa Haut-Kramel" ["Sandrine";"Etienne";"Pierre";"Stephane";"Julien"] ;
ajoute_commission session_attente ~nom_formation:"Prepa Eau-el" ["Sandrine"];
reunit_commissions session_attente;
nouveau_jour session_attente;
affiche_voeux_en_attente session_attente "Sandrine";
affiche_propositions_en_attente session_attente "Sandrine";
affiche_propositions_en_attente session_attente "Etienne";
affiche_propositions_en_attente session_attente "Pierre";
affiche_voeux_en_attente session_attente "Pierre";
affiche_propositions_en_attente session_attente "Stephane";
affiche_voeux_en_attente session_attente "Stephane";
affiche_propositions_en_attente session_attente "Julien";
affiche_voeux_en_attente session_attente "Julien";
nouveau_jour session_attente;
affiche_voeux_en_attente session_attente "Sandrine";
affiche_propositions_en_attente session_attente "Sandrine";
affiche_propositions_en_attente session_attente "Etienne";
affiche_propositions_en_attente session_attente "Pierre";
affiche_voeux_en_attente session_attente "Pierre";
affiche_propositions_en_attente session_attente "Stephane";
affiche_voeux_en_attente session_attente "Stephane";
affiche_propositions_en_attente session_attente "Julien";
affiche_voeux_en_attente session_attente "Julien"