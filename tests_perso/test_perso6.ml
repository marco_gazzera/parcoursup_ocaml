open Utils
(*
Propriéte testé : Ajout proposition de même rang / suppression de plusieurs propositions / suppression de voeux en attente 

Objectif : Ici Sandrine et Etienne postule dans la même formation, qui ne prendra qu'Etienne, ainsi Sandrine est en attente sur tout ses voeux.
          Etienne va refusé ses voeux dans le rang est None au fur et à mesure des jours, et on vérifie que Sandrine met bien à jour sa liste d'attente et ajoute 
          les voeux à sa liste de propositions, au moment d'acecpté "Prepa Serpent à Sonette" Sandrine doit supprimer tout ses voeux de rang 1 pour ne prendre que "Prepa Serpent à Sonette".
          Etienne doit terminer avec Prepa Twitch, seul formation a laquel il ne renonce pas


Remarque :   Après que Etienne ai refusé Prepa Haut-Kramel on reaffiche les proposition/attente des deux candidats, on voit bien que Sandrine n'a pas encore reçu la proposition de 
              Prepa Haut-Kramel, car il faut un nouveau_jour pour l'appelé, et Etienne a bien supprimé Prepa Haut-Kramel de sa liste*)
let session_cumule_rang = nouvelle_session ()
  
let () =
  ajoute_candidat session_cumule_rang ~nom_candidat:"Sandrine";
  ajoute_candidat session_cumule_rang ~nom_candidat:"Etienne" ;
  ajoute_formation session_cumule_rang ~nom_formation:"Prepa Haut-Kramel" ~capacite:1;
  ajoute_formation session_cumule_rang ~nom_formation:"Prepa Eau-el" ~capacite:1;
  ajoute_formation session_cumule_rang ~nom_formation:"Prepa Serpent à Sonette" ~capacite:1;
  ajoute_formation session_cumule_rang ~nom_formation:"Prepa Twitch" ~capacite:1;
  ajoute_voeu session_cumule_rang ~rang_repondeur:(Some 1) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Haut-Kramel";
  ajoute_voeu session_cumule_rang ~rang_repondeur:(Some 1) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Eau-el";
  ajoute_voeu session_cumule_rang ~rang_repondeur:(Some 0) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Serpent à Sonette";
  ajoute_voeu session_cumule_rang ~rang_repondeur:(Some 1) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Twitch";

  ajoute_voeu session_cumule_rang ~rang_repondeur:(None) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Haut-Kramel";
  ajoute_voeu session_cumule_rang ~rang_repondeur:(None) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Eau-el";
  ajoute_voeu session_cumule_rang ~rang_repondeur:(None) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Serpent à Sonette";
  ajoute_voeu session_cumule_rang ~rang_repondeur:(Some 1) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Twitch";

  ajoute_commission session_cumule_rang ~nom_formation:"Prepa Haut-Kramel" ["Etienne";"Sandrine"];
  ajoute_commission session_cumule_rang ~nom_formation:"Prepa Eau-el" ["Etienne";"Sandrine"];
  ajoute_commission session_cumule_rang ~nom_formation:"Prepa Serpent à Sonette" ["Etienne";"Sandrine"];
  ajoute_commission session_cumule_rang ~nom_formation:"Prepa Twitch" ["Etienne";"Sandrine"];
  
  reunit_commissions session_cumule_rang;
  nouveau_jour session_cumule_rang;

affiche_voeux_en_attente session_cumule_rang "Sandrine";
affiche_propositions_en_attente session_cumule_rang "Sandrine";

affiche_voeux_en_attente session_cumule_rang "Etienne";
affiche_propositions_en_attente session_cumule_rang "Etienne";

renonce session_cumule_rang ~nom_candidat:"Etienne" ~nom_formation:"Prepa Haut-Kramel";
affiche_voeux_en_attente session_cumule_rang "Etienne";
affiche_propositions_en_attente session_cumule_rang "Etienne";
affiche_voeux_en_attente session_cumule_rang "Sandrine";
affiche_propositions_en_attente session_cumule_rang "Sandrine";
renonce session_cumule_rang ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Twitch";
nouveau_jour session_cumule_rang;

affiche_voeux_en_attente session_cumule_rang "Sandrine";
affiche_propositions_en_attente session_cumule_rang "Sandrine";

affiche_voeux_en_attente session_cumule_rang "Etienne";
affiche_propositions_en_attente session_cumule_rang "Etienne";

renonce session_cumule_rang ~nom_candidat:"Etienne" ~nom_formation:"Prepa Eau-el";
nouveau_jour session_cumule_rang;
affiche_voeux_en_attente session_cumule_rang "Sandrine";
affiche_propositions_en_attente session_cumule_rang "Sandrine";

affiche_voeux_en_attente session_cumule_rang "Etienne";
affiche_propositions_en_attente session_cumule_rang "Etienne";

renonce session_cumule_rang ~nom_candidat:"Etienne" ~nom_formation:"Prepa Serpent à Sonette";
nouveau_jour session_cumule_rang;
affiche_voeux_en_attente session_cumule_rang "Sandrine";
affiche_propositions_en_attente session_cumule_rang "Sandrine";

affiche_voeux_en_attente session_cumule_rang "Etienne";
affiche_propositions_en_attente session_cumule_rang "Etienne";
