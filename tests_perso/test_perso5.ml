open Utils
let session_parfaite = nouvelle_session ()
let session_parfaite_bis = nouvelle_session ()
(*
Propriéte testé : changement de voeu 

Objectif : Même test que test2 SAUF que Sandrine nous utilisons 2 sessions différentes, on voit bien que Sandrine de la 
        session_parfaite fini bien en Prepa Haut-Kramel et Sandrine de la session_parfaite_bis fini bien en Prepa Eau-El

Remarque :Tout se déroule comme prévue.*)
let () =
  
ajoute_candidat session_parfaite ~nom_candidat:"Sandrine";
ajoute_candidat session_parfaite ~nom_candidat:"Etienne" ;
ajoute_candidat session_parfaite ~nom_candidat:"Pierre" ;

ajoute_candidat session_parfaite_bis ~nom_candidat:"Sandrine";

ajoute_formation session_parfaite ~nom_formation:"Prepa Haut-Kramel" ~capacite:1000;
ajoute_formation session_parfaite ~nom_formation:"Prepa Eau-el" ~capacite:100;
ajoute_formation session_parfaite ~nom_formation:"Prepa Serpent à Sonette" ~capacite:100;
ajoute_formation session_parfaite_bis ~nom_formation:"Prepa Haut-Kramel" ~capacite:1000;
ajoute_formation session_parfaite_bis ~nom_formation:"Prepa Eau-el" ~capacite:100;
ajoute_formation session_parfaite_bis ~nom_formation:"Prepa Serpent à Sonette" ~capacite:100;
ajoute_voeu session_parfaite ~rang_repondeur:(Some 0) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 1) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 2) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Serpent à Sonette";

ajoute_voeu session_parfaite_bis ~rang_repondeur:(Some 1) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_parfaite_bis ~rang_repondeur:(Some 0) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_parfaite_bis ~rang_repondeur:(Some 2) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Serpent à Sonette";

ajoute_voeu session_parfaite ~rang_repondeur:(Some 1) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 0) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 2) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Serpent à Sonette";

ajoute_voeu session_parfaite ~rang_repondeur:(Some 2) ~nom_candidat:"Pierre" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 1) ~nom_candidat:"Pierre" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 0) ~nom_candidat:"Pierre" ~nom_formation:"Prepa Serpent à Sonette";

ajoute_commission session_parfaite ~nom_formation:"Prepa Haut-Kramel" ["Etienne";"Pierre";"Sandrine"] ;
ajoute_commission session_parfaite ~nom_formation:"Prepa Eau-el" ["Pierre";"Sandrine";"Etienne"] ;
ajoute_commission session_parfaite ~nom_formation:"Prepa Serpent à Sonette" ["Pierre";"Etienne";"Sandrine"] ;
ajoute_commission session_parfaite_bis ~nom_formation:"Prepa Haut-Kramel" ["Sandrine"] ;
ajoute_commission session_parfaite_bis ~nom_formation:"Prepa Eau-el" ["Sandrine"] ;
ajoute_commission session_parfaite_bis ~nom_formation:"Prepa Serpent à Sonette" ["Sandrine"] ;
reunit_commissions session_parfaite;
reunit_commissions session_parfaite_bis;

nouveau_jour session_parfaite;
nouveau_jour session_parfaite_bis;

affiche_propositions_en_attente session_parfaite "Sandrine";
affiche_voeux_en_attente session_parfaite "Sandrine";
affiche_propositions_en_attente session_parfaite_bis "Sandrine";
affiche_voeux_en_attente session_parfaite_bis "Sandrine";

affiche_propositions_en_attente session_parfaite "Etienne";
affiche_voeux_en_attente session_parfaite "Etienne";

affiche_propositions_en_attente session_parfaite "Pierre";
affiche_voeux_en_attente session_parfaite "Pierre"