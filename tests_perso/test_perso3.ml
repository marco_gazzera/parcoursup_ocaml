open Utils
let session_sans_eleves = nouvelle_session ()
(*
Propriéte testé : 
Objectif : Vérifié que le programme ne crash pas pour une formation sans étudiant


Remarque :Il ne se passe rien, comme prévu..*)
let () =
ajoute_formation session_sans_eleves ~nom_formation:"Prepa Haut-Kramel" ~capacite:1000;
ajoute_commission session_sans_eleves ~nom_formation:"Prepa Haut-Kramel" [] ;
reunit_commissions session_sans_eleves;
nouveau_jour session_sans_eleves