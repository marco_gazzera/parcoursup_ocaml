open Utils
let session_parfaite = nouvelle_session ()
(*
Propriéte testé : renonce_automatique / ajout proposition 

Objectif : Les élèves se voient proposé toutes les formations demandé car la capacité est suffisante, tous refuse les propositions inférieur
          à leur meilleur voeux et obtiennent leur voeux


Remarque :Tout se déroule comme prévue.*)
let () =
  
ajoute_candidat session_parfaite ~nom_candidat:"Sandrine";
ajoute_candidat session_parfaite ~nom_candidat:"Etienne" ;
ajoute_candidat session_parfaite ~nom_candidat:"Pierre" ;

ajoute_formation session_parfaite ~nom_formation:"Prepa Haut-Kramel" ~capacite:1000;
ajoute_formation session_parfaite ~nom_formation:"Prepa Eau-el" ~capacite:100;
ajoute_formation session_parfaite ~nom_formation:"Prepa Serpent à Sonette" ~capacite:100;
ajoute_voeu session_parfaite ~rang_repondeur:(Some 0) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 1) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 2) ~nom_candidat:"Sandrine" ~nom_formation:"Prepa Serpent à Sonette";

ajoute_voeu session_parfaite ~rang_repondeur:(Some 1) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 0) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 2) ~nom_candidat:"Etienne" ~nom_formation:"Prepa Serpent à Sonette";

ajoute_voeu session_parfaite ~rang_repondeur:(Some 2) ~nom_candidat:"Pierre" ~nom_formation:"Prepa Haut-Kramel";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 1) ~nom_candidat:"Pierre" ~nom_formation:"Prepa Eau-el";
ajoute_voeu session_parfaite ~rang_repondeur:(Some 0) ~nom_candidat:"Pierre" ~nom_formation:"Prepa Serpent à Sonette";

ajoute_commission session_parfaite ~nom_formation:"Prepa Haut-Kramel" ["Etienne";"Pierre";"Sandrine"] ;
ajoute_commission session_parfaite ~nom_formation:"Prepa Eau-el" ["Pierre";"Sandrine";"Etienne"] ;
ajoute_commission session_parfaite ~nom_formation:"Prepa Serpent à Sonette" ["Pierre";"Etienne";"Sandrine"] ;

reunit_commissions session_parfaite;

nouveau_jour session_parfaite;

affiche_propositions_en_attente session_parfaite "Sandrine";
affiche_voeux_en_attente session_parfaite "Sandrine";

affiche_propositions_en_attente session_parfaite "Etienne";
affiche_voeux_en_attente session_parfaite "Etienne";

affiche_propositions_en_attente session_parfaite "Pierre";
affiche_voeux_en_attente session_parfaite "Pierre"