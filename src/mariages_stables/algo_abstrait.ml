open Definitions

module type PIOCHE = sig
  type 'a t
    val of_list: 'a list -> 'a t 
    val pioche : 'a t -> 'a option 
    val defausse : 'a -> 'a t -> unit 
end

module Pile : PIOCHE = struct
  type 'a t =   'a option list ref (* <- à modifier *)
  let of_list l = match l with
  | [] -> ref []
  | hd :: tl -> ref (List.map (fun i -> Some(i)) (hd::tl) )  
  let pioche p = match !p with
     [] -> None
    | hd::tl  -> begin
      p := tl ;
      hd
    end
  let defausse x p =
    p := Some(x):: !p
end

module File : PIOCHE = struct
  type 'a t =  {mutable front: 'a option list ;mutable rear : 'a option list }      (* <- à modifier *)
  let of_list l = match l with
  | []-> {front = []; rear = []}
  | hd::tl -> { front = ( List.map (fun i -> Some(i)) (hd::tl) )  ; rear = ( List.rev ( List.map (fun i -> Some(i)) (hd::tl) ) )}
  let pioche p = match p.front with
  | [] -> None
  | hd::tl -> begin 
    p.rear <- List.rev tl;
    p.front <- tl;
    hd
  end
  let defausse x p =
    p.rear <- Some(x) :: p.rear;
    p.front <- List.rev p.rear;
end

module Algo(P:PIOCHE) = struct
  let run entree =
    let n = entree.n in
    let option_to_int = function
    | None -> 0
    | Some n -> n
    in
  (*let deepcopy (v:'a) : 'a = Marshal.from_string (Marshal.to_string v []) 0 in *) (* faire des copies en ocaml *)
  let pretendants = Array.make n [||] in 
  let omega : int option = None in
  let config = {rang_appel_de = Array.make n 0;fiance_de = Array.make n omega }in
  let la_pioche = P.of_list (List.init n (fun i -> i)) in
  let lomme : int option ref = ref (P.pioche la_pioche) in 
  let lafamme = ref 0 in
  while (!lomme != None) do
    lafamme := entree.liste_appel_de.(option_to_int !lomme).(0);
    if (config.fiance_de.(!lafamme)=None) then (config.fiance_de.(!lafamme)<- !lomme;)
    else (
      pretendants.(!lafamme) <- Array.append pretendants.(!lafamme) (Array.make 1 (option_to_int !lomme));
      pretendants.(!lafamme) <- Array.append pretendants.(!lafamme) (Array.make 1 (option_to_int config.fiance_de.(!lafamme)));
      (*  pretendants.(2) <- Array.append pretendants.(2) (Array.make 1 3);
      pretendants.(2) <- Array.append pretendants.(2) (Array.make 1 0 ); *)
      if ( entree.prefere.(!lafamme) pretendants.(!lafamme).(0) pretendants.(!lafamme).(1) ) then ( (*entree.prefere.(2) pretendants.(2).(0) pretendants.(2).(1) *)
        entree.liste_appel_de.(pretendants.(!lafamme).(1)) <- Array.sub (entree.liste_appel_de.(pretendants.(!lafamme).(1))) 1 ( ( Array.length entree.liste_appel_de.(pretendants.(!lafamme).(1)) -1 ));
        config.rang_appel_de.(pretendants.(!lafamme).(1)) <- config.rang_appel_de.(pretendants.(!lafamme).(1)) +1;
        P.defausse pretendants.(!lafamme).(1) la_pioche;
        config.fiance_de.(!lafamme) <-Some( pretendants.(!lafamme).(0) );
        pretendants.(!lafamme) <- [||];
      )
      else (
        entree.liste_appel_de.(pretendants.(!lafamme).(0)) <- Array.sub (entree.liste_appel_de.(pretendants.(!lafamme).(0))) 1 ( ( Array.length entree.liste_appel_de.(pretendants.(!lafamme).(0)) -1 ));
        config.rang_appel_de.(pretendants.(!lafamme).(0)) <- config.rang_appel_de.(pretendants.(!lafamme).(0)) +1;
        P.defausse pretendants.(!lafamme).(0) la_pioche;
        pretendants.(!lafamme) <- [||];
      ););

      lomme := P.pioche la_pioche;
  done;
  List.init n (fun i -> ( i , entree.liste_appel_de.(i).(0) ))

end
     
