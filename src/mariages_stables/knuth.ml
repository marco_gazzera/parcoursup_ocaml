 (*dune runtest test/mariages_stables/knuth*)
open Definitions 
let algo ?affiche_config (entree):sortie =
let n = entree.n in           (* constante n : nombre d'hommes = nombre de femmes*)
let k = ref 0 in      (* variable k : nombre de couples fiancés*)
let xX : int option ref = ref None in
let x = ref 0 in  (* variable x : femme à qui le prétendant fait une avance *)
let deepcopy (v:'a) : 'a = Marshal.from_string (Marshal.to_string v []) 0 in  (* faire des copies en ocaml c relou *)
let xXTmp = function 
| None -> 0 
| Some n -> n
    in
let omega : int option   =  None in     (* variable omega :* constante Ω : homme très indésirable *)
let config = {rang_appel_de = Array.make n 0 ;fiance_de = Array.make n omega } in
    while !k < n do 
        xX := Some (!k);
        while (!xX != omega) do
           x := entree.liste_appel_de.(xXTmp !xX).(0);
          if  ( ( config.fiance_de.(!x)=None)||( entree.prefere.(!x) (xXTmp !xX ) (xXTmp config.fiance_de.(!x))  ))  then (
           let tmp = deepcopy config.fiance_de.(!x) in
           config.fiance_de.(!x) <- !xX;
           xX := tmp;
            );
            if !xX != omega then (  
            entree.liste_appel_de.(xXTmp !xX) <- Array.sub (entree.liste_appel_de.(xXTmp !xX) ) 1 ( ( Array.length entree.liste_appel_de.(xXTmp !xX) ) -1 ) ;
            config.rang_appel_de.(xXTmp !xX) <-  (config.rang_appel_de.(xXTmp !xX)+1);
                  );
              if affiche_config = Some true then print_configuration config;
              done;
        k := !k+1;
    done;
 List.init n (fun i -> ( i , entree.liste_appel_de.(i).(0) ))