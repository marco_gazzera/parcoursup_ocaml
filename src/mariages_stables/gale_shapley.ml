 (*dune runtest test/mariages_stables/gale_shapley*)
open Definitions 
let algo ?affiche_config (entree):sortie =
let n = entree.n in           (* constante n : nombre d'hommes = nombre de femmes*)   (* variable k : nombre de couples fiancés*)
let xX : int option ref = ref None in
let x = ref 0 in  (* variable x : femme à qui le prétendant fait une avance *)
let deepcopy (v:'a) : 'a = Marshal.from_string (Marshal.to_string v []) 0 in  (* faire des copies en ocaml *)
let xXTmp = function 
| None -> 0 
| Some n -> n
    in
let omega : int option   =  None in     (* variable omega :* constante Ω : homme très indésirable *)


let config = {rang_appel_de = Array.make n 0 ;fiance_de = Array.make n omega } in
let pretendants = Array.make n  [||] in  (*                         Liste des pretendants de A : None 
                                                                    Liste des pretendants de B : B C
                                                                    Liste des pretendants de C: C A
                                                                    Liste des pretendants de D : None *)
let rang_appelBis = Array.make n (-1) in
(* tester s'il reste des celibataires par exemple en cherchant les None *)
(* ou en verifiant la taille du tableau ou une structure *)
while ( Array.exists ( fun x -> x = -1 ) rang_appelBis) do

     for j = 0 to n - 1 do
        if (rang_appelBis.(j)=(-1) ) then ( (* pour tout homme celibataire donc dans le tableau *)
            x := entree.liste_appel_de.(j).(0);
            pretendants.(!x) <- Array.append pretendants.(!x) [|j|];); (*[ [] ; [1;2]; [0;3]; [] ]   pretendants.(!x) @ [ i ]*)
     done; 
     
        for i = 0 to n - 1 do  if ( ( pretendants.(i)!=[||])) then (
                (* X := pretendants que x prefere *)
               (* Array.iter (fun i -> Format.printf "Pretendants : %d" i) pretendants.(i) ;*)
                while( Array.length pretendants.(i)>1) do
                   
                    if ( entree.prefere.(i) pretendants.(i).(0) pretendants.(i).(1) ) then (
                        let tmp = deepcopy pretendants.(i).(0) in
                        pretendants.(i).(0)<-pretendants.(i).(1);
                        pretendants.(i).(1)<-tmp;
                    );
                    config.rang_appel_de.(pretendants.(i).(0))<-(config.rang_appel_de.(pretendants.(i).(0)) +1 ) ;
                    entree.liste_appel_de.(pretendants.(i).(0))<-Array.sub (entree.liste_appel_de.(pretendants.(i).(0) )) 1 ( ( Array.length entree.liste_appel_de.(pretendants.(i).(0) ) -1 ));
                    pretendants.(i) <- Array.sub (pretendants.(i) ) 1 ( ( Array.length pretendants.(i)) -1 ) ;
                done;
            xX := Some(pretendants.(i).(0));
           (* Format.printf " ( X = %d ) avec i = %d: \n" (xXTmp !xX) i;*)
            if  ( ( config.fiance_de.(i)=None)||( entree.prefere.(i) (xXTmp !xX ) (xXTmp config.fiance_de.(i))  ))  then ( 
                if (config.fiance_de.(i) != None ) then ( (*divorce de l'ancien *)
                    let tmp = config.fiance_de.(i) in
                    rang_appelBis.(xXTmp tmp) <- (-1);
                    config.rang_appel_de.(xXTmp tmp)<- (config.rang_appel_de.(xXTmp tmp) +1 ) ;
                    entree.liste_appel_de.(xXTmp tmp)<-Array.sub (entree.liste_appel_de.(xXTmp tmp )) 1 ( ( Array.length entree.liste_appel_de.(xXTmp tmp ) -1 )) ;
                 );
                
            config.fiance_de.(i) <- !xX;
            rang_appelBis.(xXTmp !xX)<-0;
            )
            else(
                config.rang_appel_de.(xXTmp !xX)<- (config.rang_appel_de.(xXTmp !xX) +1 ) ;
                entree.liste_appel_de.(xXTmp !xX)<-Array.sub (entree.liste_appel_de.(xXTmp !xX )) 1 ( ( Array.length entree.liste_appel_de.(xXTmp !xX ) -1 )) ;
            );
        pretendants.(i)<- [||] ;
            );done;
        if affiche_config = Some true then print_configuration config;
done;
List.init n (fun i -> ( i , entree.liste_appel_de.(i).(0) ))