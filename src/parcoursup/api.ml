type voeux = {
    rang : int option ;
    formation : string;
}
;;
type candidats = {
nom : string;
voeux : voeux array ref ;
liste_voeux_propose : string list ref; (* int = le numéro dans la liste d'attente *)
liste_voeux_en_attente : (string * int) list ref;
}
;;
type formation = {
    nom : string;
    capacite : int;
    nbr_appelee : int ref;
    liste_pretendants : string list ref;
    array_post_commission : string list ref;
    mutable liste_classee : string list lazy_t;
    indice_appel : int ref;
}
;;
type session = {
liste_candidats :  candidats array ref;
liste_formation :  formation array ref;
}
;;
let nouvelle_session () = 
  {
  liste_candidats = ref [||];
  liste_formation = ref [||];
  }
;;
let ajoute_candidat session ~nom_candidat = 
 let candidats = {
   nom=nom_candidat;
   voeux = ref [||];
   liste_voeux_propose = ref [];
   liste_voeux_en_attente = ref [];
   } in
   let pointeur =  session.liste_candidats in
   session.liste_candidats := Array.append !pointeur (Array.make 1 candidats) ; 
;;
let ajoute_formation session ~nom_formation ~capacite =
 let formation = {
   nom = nom_formation;
   capacite = capacite;
   nbr_appelee = ref 0;
   liste_pretendants = ref [];
   array_post_commission = ref [];
   liste_classee =  lazy([]) ;
   indice_appel = ref 0;
 } in
 let variable_temporaire_inutile_pour_pouvoir_dereferencer = session.liste_formation in
 session.liste_formation := Array.append !variable_temporaire_inutile_pour_pouvoir_dereferencer (Array.make 1 formation);
 ;;

  let ajoute_voeu session ~rang_repondeur ~nom_candidat ~nom_formation = 
    let voeux = {
      rang = rang_repondeur;
      formation = nom_formation;
    } in
    let les_candidats = session.liste_candidats in
    let incr = ref 0 in
    while (!les_candidats.(!incr).nom <> nom_candidat ) do
      incr := !incr + 1;  (* on trouve le numero de l'nom_candidat dans le tableau des nom_candidat *)
    done;
    let le_candidat = !les_candidats.(!incr) in
    let les_voeux = le_candidat.voeux in
    let length_des_voeux = Array.length !les_voeux in
    if( length_des_voeux = 0 ) then (
      les_voeux := Array.make 1 voeux; 
      let incr4 = ref 0 in
      let les_formations = session.liste_formation in
      while ( ( !incr4 < (Array.length !les_formations) - 1) && ( nom_formation <> !les_formations.(!incr4).nom )) do
        incr4 := !incr4 +1;
      done;
      let la_formation = !les_formations.(!incr4) in
      let les_pretendants_de_la_formation = la_formation.liste_pretendants in
      les_pretendants_de_la_formation := List.append !les_pretendants_de_la_formation [nom_candidat];(* trouve le numero du voeux *)
    )
    else (
      let incr2 = ref 0 in
    while (   ( !incr2 < length_des_voeux ) && ( !les_voeux.(!incr2).formation <> nom_formation )  ) 
    do
      incr2 := !incr2 +1;
    done;
    if ( !incr2 <= length_des_voeux - 1 && !les_voeux.(!incr2).formation = nom_formation ) then (
      !les_voeux.(!incr2) <- voeux;
    )
    else
    (
      les_voeux := Array.append !les_voeux ( Array.make 1 voeux ); (*on rajoute le nouveau voeu *)
      let incr3 = ref 0 in
      let les_formations = session.liste_formation in
      while ( ( !incr3 < (Array.length !les_formations) - 1) && ( nom_formation <> !les_formations.(!incr3).nom )) do
        incr3 := !incr3 +1;
      done;
      let la_formation = !les_formations.(!incr3) in
      let les_pretendants_de_la_formation = la_formation.liste_pretendants in
      les_pretendants_de_la_formation := List.append !les_pretendants_de_la_formation [nom_candidat];
    );
    );
;;
let ajoute_commission session ~nom_formation ~fonction_comparaison  = 
let incr1 = ref 0 in
let les_formations = session.liste_formation in
while ( ( !incr1 < (Array.length !les_formations) - 1) && ( nom_formation <> !les_formations.(!incr1).nom )) do
  incr1 := !incr1 +1;
done;
let la_formation = !les_formations.(!incr1) in
let les_pretendants_de_la_formation = la_formation.liste_pretendants in
la_formation.liste_classee <- lazy(List.sort (fun candidat1 candidat2 -> if ( candidat1 = candidat2 ) then 0 else if (fonction_comparaison ~candidat1 ~candidat2) then -1 else 1 ) !les_pretendants_de_la_formation ); 
(*En classant les élèves dans un Lazy, nous pouvons continuer de rajouté des élèves après avoir la commission. Ce qui s'avère inutile puisque pas demandé ...*)
(*les_pretendants_de_la_formation := List.sort (fun i j ->compare  i j) !les_pretendants_de_la_formation ; *)
;;

let reunit_commissions session =
    let les_formations = session.liste_formation in
  let la_formation = ref !les_formations.(0) in  (* ocaml a besoin d'aide pour comprendre le type donc on l'aide à démarrer *)
  let pour_faire_plaisir_a_ocaml = Lazy.force (!la_formation.liste_classee) in (*Ocaml considère faux de faire Lazy.force (truc) quand bien meme nous avons besoin uniquement de changé la valeur de truc avec un Lazy.force *)
  ignore pour_faire_plaisir_a_ocaml; (* il nous faut donc le mettre dans une variable, inutile, et ensuite il nous faut dire au compilo que cette variable est inutile, donc on l'ignore ... reviens à faire Lazy.force(truc), ce qui marche sur le terminal, mais pas avec le compilateur :facepalm: *)
  !la_formation.array_post_commission := List.init (List.length ((Lazy.force (!la_formation.liste_classee)))) (fun i -> (List.nth (Lazy.force (!la_formation.liste_classee)) i));
  for i=1 to (Array.length !les_formations) -1 do
    la_formation := !les_formations.(i);
    let pour_faire_plaisir_a_ocaml = Lazy.force (!la_formation.liste_classee) in
    ignore pour_faire_plaisir_a_ocaml;
    !la_formation.array_post_commission :=  List.init (List.length ((Lazy.force (!la_formation.liste_classee)))) (fun i -> (List.nth (Lazy.force (!la_formation.liste_classee)) i));
  done;
;;


let nouveau_jour session =
  let deref laref = !laref in (*A = ref 0 , B = ref B , !B = ref, deref !B = 0. Comme nos structure de donné sont composé de nombreuse ref, nous avons souvent besoin de dereferencer deux fois hors, il n'est pas juste syntaxiquement d'écrire !!B, on écris donc deref !B*)
  let les_formations = session.liste_formation in
  let les_candidats = session.liste_candidats in
  let renonce_automatique nom_candidat nom_formation = (*cette fonction n'est appelé que dans ajout_proposition, mais pas forcement avec le même nom_formation donc pour évité un malentendu je la déclare en dehors *)
    let incr2 = ref 0 in
    while(!les_formations.(!incr2).nom <> nom_formation) do
      incr2 := !incr2 +1 ;
    done;
    let la_formation = ref !les_formations.(!incr2) in (* erreur *)
     !la_formation.nbr_appelee := (deref !la_formation.nbr_appelee) -1;
     !la_formation.array_post_commission  := List.filter (fun i -> i <> nom_candidat ) (deref !la_formation.array_post_commission ) ; 
     in
  let ajout_proposition nom_candidat nom_formation = 
    let incr = ref 0 in
    while (!les_candidats.(!incr).nom <> nom_candidat ) do
      incr := !incr + 1;  (* on trouve le numero de l'nom_candidat dans le tableau des nom_candidat *)
    done;
    let les_voeux = (deref !les_candidats.(!incr).voeux) in
    let incr2 = ref 0 in
    while(les_voeux.(!incr2).formation <> nom_formation ) do  (* incr =  numero candidat ; incr2 = numero de la nouvelle formation ; incr 3 = indice de incr4 dans la liste *)
      incr2 := !incr2 + 1 ;
    done;
    let rang_new_formation = les_voeux.(!incr2).rang in
    if ( rang_new_formation = None || (List.length (deref !les_candidats.(!incr).liste_voeux_propose))=0 ) then( (*Si le rang de la nouvelle formation est None, ou si il n'y a aucune formation de proposé alors on ajoute cette formation*)
      !les_candidats.(!incr).liste_voeux_propose := List.append (deref !les_candidats.(!incr).liste_voeux_propose) [nom_formation];
      !les_candidats.(!incr).liste_voeux_en_attente := List.filter (fun i -> (fst i) <> nom_formation) (deref !les_candidats.(!incr).liste_voeux_en_attente);
      )
    else(
      let incr4 = ref 0 in (* indice de incr3 dans les voeux *)
      let is_none nom_formation_a_verifier incr4 = (*Cette fonction renvoie true si le rang de la formation regardé est None, car si c'est le cas on ne doit pas la compter lorsqu'il s'agit de controler les rang des formations du candidats en vue du répondeur automatique*)
        while(les_voeux.(!incr4).formation <> !nom_formation_a_verifier) do
            incr4 := !incr4 + 1;
          done;
          if(les_voeux.(!incr4).rang = None ) then true  else false ;
        in
      let incr3 = ref 0 in 
      let la_formation_a_verifier_depuis_liste_propo = ref ( List.nth (deref !les_candidats.(!incr).liste_voeux_propose) !incr3 ) in
      while ( ( is_none la_formation_a_verifier_depuis_liste_propo incr4 ) && ( !incr3 < (List.length (deref !les_candidats.(!incr).liste_voeux_propose)) -1 ) ) do
        incr4 := 0;(*Cette boucle parcours la liste des propositions pour trouver une formation dont le rang est none, une fois trouvé elle se termine et incr4 vaudra l'indice de cette formation*)
        incr3 := !incr3 + 1;
        la_formation_a_verifier_depuis_liste_propo :=  List.nth (deref !les_candidats.(!incr).liste_voeux_propose) !incr3 ;
      done;
      let rang_deja_present = les_voeux.(!incr4).rang in
      if ( rang_deja_present <> None ) then ( (*La boucle a pu se termine sans trouver de rang=None, on vérifie cela*)
        if(rang_new_formation > rang_deja_present) then ( (*Si le rang de la nouvelle formation est supérieur à celui déjà présent, c'est qu'on doit le refusé automatiquement.*)
          renonce_automatique nom_candidat nom_formation;
          !les_candidats.(!incr).liste_voeux_en_attente := List.filter (fun i -> (fst i) <> nom_formation) (deref !les_candidats.(!incr).liste_voeux_en_attente);
          (*!incr3 < ( List.length (deref !les_candidats.(!incr).liste_voeux_propose)) )-1 *)
        ) else
        (
          if(rang_new_formation = rang_deja_present) then (*Si le rang est égal on doit le rajouté à notre liste*)
          (
            !les_candidats.(!incr).liste_voeux_propose := List.append (deref !les_candidats.(!incr).liste_voeux_propose) [nom_formation];
            !les_candidats.(!incr).liste_voeux_en_attente := List.filter (fun i -> (fst i) <> nom_formation) (deref !les_candidats.(!incr).liste_voeux_en_attente);
            )
          else(
            if(List.length (deref !les_candidats.(!incr).liste_voeux_propose) = 1 ) then (*Si il n'y a qu'une seul proposition, et que celle ci n'est pas égal à la nouvelle formation, alors on l'a renonce pour mettre notre nouvelle*)
            (
              renonce_automatique nom_candidat  (List.nth (deref !les_candidats.(!incr).liste_voeux_propose) 0);
              !les_candidats.(!incr).liste_voeux_propose :=[nom_formation] ; (*Il n'y a qu'une seul et unique formation proposé, pas besoin de search and destroy toutes les 'autres' formations, puisqu'elle est seul *)
              !les_candidats.(!incr).liste_voeux_en_attente := List.filter (fun i -> (fst i) <> nom_formation) (deref !les_candidats.(!incr).liste_voeux_en_attente);
             
              )else(
              (* search and destroy *) (*Cette boucle va supprimer l'intégralité des formations actuellement proposé que ne conviennent pas à tout les test précedents, à savoir, toutes les propositions dont le rang est supérieur a  celui de la nouvelle formation ET qui sont différent de None*)
            let incr5 = ref 0 in (* nouvelle indice pareil que pour le 3 *)
            la_formation_a_verifier_depuis_liste_propo :=  List.nth (deref !les_candidats.(!incr).liste_voeux_propose) !incr5 ;
            while ( !incr5 <= ( List.length (deref !les_candidats.(!incr).liste_voeux_propose) )-1  ) do
              incr4 := 0;
            if( (is_none la_formation_a_verifier_depuis_liste_propo incr4) <> true ) then (
              !les_candidats.(!incr).liste_voeux_propose := List.filter (fun i -> i <> !la_formation_a_verifier_depuis_liste_propo ) (deref !les_candidats.(!incr).liste_voeux_propose);
              renonce_automatique nom_candidat !la_formation_a_verifier_depuis_liste_propo; (*Ici il faut aussi renoncer à cette formation !*)
              !les_candidats.(!incr).liste_voeux_en_attente := List.filter (fun i -> (fst i) <> nom_formation) (deref !les_candidats.(!incr).liste_voeux_en_attente);
              if ( !incr5 <= (List.length (deref !les_candidats.(!incr).liste_voeux_propose ))-1) then (
                la_formation_a_verifier_depuis_liste_propo :=  List.nth (deref !les_candidats.(!incr).liste_voeux_propose) !incr5 ;
              );
            )else(
              incr5 := !incr5 +1 ;
              la_formation_a_verifier_depuis_liste_propo :=  List.nth (deref !les_candidats.(!incr).liste_voeux_propose) !incr5 ;
            );
            done;
            (* search and destroy *)
            !les_candidats.(!incr).liste_voeux_propose := List.append (deref !les_candidats.(!incr).liste_voeux_propose) [nom_formation];
        );););
      )else(
        !les_candidats.(!incr).liste_voeux_propose := List.append (deref !les_candidats.(!incr).liste_voeux_propose) [nom_formation];
      );
      );
     in
  let ajout_attente nom_candidat nom_formation numero= 
    let incr = ref 0 in
    while (!les_candidats.(!incr).nom <> nom_candidat ) do
      incr := !incr + 1;  (* on trouve le numero de l'nom_candidat dans le tableau des nom_candidat *)
    done;
    !les_candidats.(!incr).liste_voeux_en_attente := List.filter (fun i -> (fst i) <> nom_formation) (deref !les_candidats.(!incr).liste_voeux_en_attente);
    !les_candidats.(!incr).liste_voeux_en_attente := List.append (deref !les_candidats.(!incr).liste_voeux_en_attente) ([(nom_formation,numero-1)]); in
  let gestion_formation formation  = (*Gestion_formation s'execute sur chacune des formations de la session, on commence par appelé les élèves de la liste array_post_commission 
                                      les élèves sont appelé jusqu'a ce que la capacité d'élèves de la formation soit atteinte par nbr_appelé, si les deux sont égaux, alors aucun élève n'est appelé*)
    let la_liste = !formation.array_post_commission in
    while( ( deref !formation.nbr_appelee <= (!formation.capacite)-1 ) &&  ( deref !formation.nbr_appelee <= ( List.length (!la_liste) ) -1 ) )do
      ajout_proposition ( List.nth (!la_liste) (deref !formation.nbr_appelee) ) !formation.nom ;
      !formation.nbr_appelee :=  (deref !formation.nbr_appelee) +1;
      !formation.indice_appel := ( deref !formation.nbr_appelee ) ;
    done;
   while ( deref !formation.indice_appel <= ( List.length !la_liste)-1 ) do (*On ajoute à la file d'attente tout les élèves qui ne sont pas appelé*)
      ajout_attente (List.nth (!la_liste) (deref !formation.indice_appel) ) !formation.nom ((deref !formation.indice_appel)-( (deref !formation.nbr_appelee) -1));
      !formation.indice_appel := (deref  !formation.indice_appel) +1;
    done; 
    in
  let la_formation = ref !les_formations.(0) in (*il s'agit d'une boucle do_while, nous avons fait celà pour que le type de la_formation soit bien considéré comme une ref sur un type session.liste_formation*)
  gestion_formation la_formation;
  for i = 1 to ((Array.length !les_formations)-1) do (*Nouveau_jour boucle uniquement sur la liste des formations, pour chaque formations nouveau_jour appelera gestion_formation*)
      la_formation := !les_formations.(i);
      gestion_formation la_formation;
  done;
;;



(*Ici la fonction renonce met à jour array_post_commission et nbr_appelee, ainsi que supprimer des liste d'attente et de propositions la fonction ciblé
Array_post_commission et nbr_appelé : Cette Liste(mal nommé donc) contient tout les élèves qui devront être appelé, dans leur ordre d'appel, nbr_appelé indique à quel élève 
l'appel s'est terminé, ainsi si notre liste contient [A;B;C;D;E] et que nbr_appelé est à 2, le dernier élève appelé était C
Si A B ou C renonce à sa formation, C est supprimé de la liste qui deviens donc [A;B;D;E] et le nbr_appelé est diminué de 1, lors du nouveau_jour, la fonction nouveau_jour va donc
faire une propositions à D pour faire revenir son nbr_appelé à 2
En fesant ainsi, nouveau_jour n'a pas besoin de recalculer sa liste d'appel à chaque execution, la liste d'appel est calculer une fois lors de réunit_commissions, puis est modifié par renonce
au fur et à mesure des renonce, renonce_automatique fait la même chose.
Dans le cas où un élève renonce à une formation encore en attente, le nbr_appelé n'est pas mis à jour puisqu'il n'a pas encore été compté comme appelé*)
let renonce session ~nom_candidat ~nom_formation = 
  let les_candidats = session.liste_candidats in
  let incr = ref 0 in
  let deref laref = !laref in
  while (!les_candidats.(!incr).nom <> nom_candidat ) do
    incr := !incr + 1;  (* on trouve le numero de l'nom_candidat dans le tableau des nom_candidat *)
  done;
  let la_liste = ref !les_candidats.(!incr).liste_voeux_propose in
  if(List.mem nom_formation (deref !la_liste))then(
    !la_liste := List.filter (fun i -> if i <> nom_formation then true else false ) (deref !la_liste);
      (*retrait de la liste de la formation *)
      let les_formations = session.liste_formation in
      let incr2 = ref 0 in
      while(!les_formations.(!incr2).nom <> nom_formation) do
        incr2 := !incr2 +1 ;
      done;
      let la_formation = ref !les_formations.(!incr2) in (* erreur *)
      !la_formation.nbr_appelee := (deref !la_formation.nbr_appelee) -1;
      !la_formation.array_post_commission  := List.filter (fun i -> i <> nom_candidat ) (deref !la_formation.array_post_commission ) )
  else(
    !les_candidats.(!incr).liste_voeux_en_attente := List.filter (fun i -> (fst i) <> nom_formation) (deref !les_candidats.(!incr).liste_voeux_en_attente);
    let les_formations = session.liste_formation in
      let incr2 = ref 0 in
      while(!les_formations.(!incr2).nom <> nom_formation) do
        incr2 := !incr2 +1 ;
      done;
      let la_formation = ref !les_formations.(!incr2) in (* erreur *)
      (*!la_formation.nbr_appelee := (deref !la_formation.nbr_appelee) -1;*)
      !la_formation.array_post_commission  := List.filter (fun i -> i <> nom_candidat ) (deref !la_formation.array_post_commission ) 
  );
;;
let consulte_propositions session ~nom_candidat =
  let les_candidats = session.liste_candidats in
  let incr = ref 0 in
  let deref laref = !laref in
  while (!les_candidats.(!incr).nom <> nom_candidat ) do
    incr := !incr + 1;  (* on trouve le numero de l'nom_candidat dans le tableau des nom_candidat *)
  done;
  deref !les_candidats.(!incr).liste_voeux_propose
  ;;


let consulte_voeux_en_attente session ~nom_candidat = 
  let les_candidats = session.liste_candidats in
  let incr = ref 0 in
  let deref laref = !laref in
  while (!les_candidats.(!incr).nom <> nom_candidat ) do
    incr := !incr + 1;  (* on trouve le numero de l'nom_candidat dans le tableau des nom_candidat *)
  done;
  deref !les_candidats.(!incr).liste_voeux_en_attente
  ;;
